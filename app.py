from flask import *
import os
import pymongo
from bson.objectid import ObjectId
from bson.binary import Binary
from bson import BSON
import datetime
from werkzeug.utils import secure_filename
from PIL import Image
from io import BytesIO
import re


DB_NAME = 'heroku_3wjg6kz7'
DB_HOST = 'ds141218.mlab.com'
DB_PORT = 41218
DB_USER = 'admin'
DB_PASS = 'password1'
myclient = pymongo.MongoClient(DB_HOST, DB_PORT)
mydb = myclient[DB_NAME]
mydb.authenticate(DB_USER, DB_PASS)

myusers = mydb['users']
mystories = mydb['stories']
myparagraphs = mydb['paragraphs']

UPLOAD_FOLDER = 'static/images'
ALLOWED_EXTENSIONS = ['png', 'jpg', 'jpeg']

app = Flask(__name__)
app.secret_key = 'ensp1-secret-key'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

class ADMIN:
    ID = '12007'
    PASS = 'root'
    TYPE = 'admin'

@app.context_processor
def override_url_for():
    return dict(
        url_for=dated_url_for
    )

def dated_url_for(endpoint, **values):
    try:
        if endpoint == 'static':
            filename = values.get('filename', None)
            if filename:
                file_path = os.path.join(
                    app.root_path,
                    endpoint, filename
                )
                values['q'] = int(os.stat(file_path).st_mtime)
        return url_for(endpoint, **values)
    except FileNotFoundError:
        return url_for("static", filename='images/logo.png')

##
# PAGES
##

@app.route('/admin')
def admin():
    if 'user_id' not in session:
        return redirect(url_for('login'))
    users = myusers.find()
    return render_template(
        'admin.html',
        users=users
    )

@app.route('/blog/<user_id>')
def blog(user_id):
    if 'user_id' in session:
        if session['user_id'] == ADMIN.ID:
            return redirect(url_for('admin'))
    user = myusers.find_one({'user_id': user_id})
    stories = mystories.find({'user_id': user_id})
    has_stories = len(list(mystories.find({'user_id': user_id}))) > 0
    for story in mystories.find({'user_id': user_id}):
        filename = f"{os.path.join(app.config['UPLOAD_FOLDER'], '{}.png'.format(story['_id']))}"
        if(not os.path.isfile(filename)):
            try:
                img = bin_to_image(story['story_img'])
                img.save(filename)
            except Exception:
                ""
    return render_template(
        "blog.html",
        user=user,
        stories=stories,
        has_stories=has_stories
    )

@app.route('/change/profile/picture')
def changeprofilepicture():
    if 'user_id' not in session:
        return redirect(url_for('login'))
    return render_template('change_profilepicture.html')

@app.route('/<user_id>/change/profile/picture/submit', methods=['POST', 'GET'])
def changeprofilepicture_submit(user_id):
    if request.method == 'POST':
        if 'file' in request.files:
            file = request.files['file']
            if file.filename != '':
                if file and check_if_allowed_file(file.filename):
                    filename = secure_filename(f'{user_id}.png')
                    filepath = os.path.join(
                        app.config['UPLOAD_FOLDER'],
                        filename
                    )
                    file.save(filepath)
                    with open(filepath, 'rb') as f:
                        encoded = Binary(f.read())
                        myusers.update_one(
                            {'user_id': user_id},
                            {'$set': {'user_img': encoded}}
                        )
    return redirect(url_for('blog', user_id=session['user_id']))

@app.route('/change/profile/password')
def changepassword():
    if 'user_id' not in session:
        return redirect(url_for('login'))
    return render_template('change_password.html')

@app.route('/changepassword/submit', methods=['POST', 'GET'])
def changepassword_submit():
    if request.method == 'POST':
        password = request.form['password']
        myusers.update_one(
            {'user_id': session['user_id']},
            {'$set': {'password': password}}
        )
    return redirect(url_for('blog', user_id=session['user_id']))

@app.route('/delete/<user_id>')
def delete(user_id):
    myusers.delete_one({'user_id': user_id})
    return redirect(url_for('admin'))

@app.route('/blog/delete/<story_id>')
def delete_story(story_id):
    if 'user_id' not in session:
        return redirect(url_for('login'))
    mystories.delete_one({'_id': ObjectId(story_id)})
    return redirect(url_for('blog', user_id=session['user_id']))

@app.route('/blog/<user_id>/<story_id>/paragraph/delete/<paragraph_id>')
def delete_paragraph(user_id, story_id, paragraph_id):
    user = myusers.find_one({'user_id': user_id})
    story = mystories.find_one({'_id': ObjectId(story_id)})
    myparagraphs.delete_one({'_id': ObjectId(paragraph_id)})
    return redirect(url_for(
        'view_blog',
        user_id=user['user_id'],
        story_id='{}'.format(story['_id']))
    )

@app.route('/blog/<user_id>/<story_id>/<paragraph_id>/edit')
def edit_paragraph(user_id, story_id, paragraph_id):
    user = myusers.find_one({'user_id': user_id})
    story = mystories.find_one({'_id': ObjectId(story_id)})
    paragraph = myparagraphs.find_one({'_id': ObjectId(paragraph_id)})
    return render_template(
        'edit_paragraph.html',
        user=user,
        story=story,
        paragraph=paragraph
    )

@app.route('/blog/<user_id>/<story_id>/<paragraph_id>/edit/submit', methods=['POST'])
def edit_paragraph_submit(user_id, story_id, paragraph_id):
    if request.method == 'POST':
        user = myusers.find_one({'user_id': user_id})
        story = mystories.find_one({'_id': ObjectId(story_id)})
        content = request.form['paragraph']
        myparagraphs.update_one(
            {'_id': ObjectId(paragraph_id)},
            {'$set': {
                'paragraph': content
            }}
        )
    return redirect(url_for(
        'view_blog',
        user_id=user['user_id'],
        story_id='{}'.format(story['_id'])
    ))

@app.route('/blog/edit/<story_id>')
def edit_story(story_id):
    if 'user_id' not in session:
        return redirect(url_for('login'))
    story = mystories.find_one({'_id': ObjectId(story_id)})
    return render_template('edit_story.html', story=story)

@app.route('/blog/edit/<story_id>/submit', methods=['POST', 'GET'])
def edit_story_submit(story_id):
    if request.method == 'POST':
        title = request.form['title']
        description = request.form['description']
        story = {
            'title': title,
            'description': description
        }
        try:
            story_type = request.form['story_type']
            if story_type != '':
                story['stype'] = story_type
        except:
            ""
        mystories.update_one(
            {'_id': ObjectId(story_id)},
            {'$set': story}
        )
        if 'file' in request.files:
            file = request.files['file']
            if file.filename != '':
                if file and check_if_allowed_file(file.filename):
                    filename = secure_filename(f'{story_id}.png')
                    filepath = os.path.join(
                        app.config['UPLOAD_FOLDER'],
                        filename
                    )
                    file.save(filepath)
                    with open(filepath, 'rb') as f:
                        encoded = Binary(f.read())
                        mystories.update_one(
                            {'_id': ObjectId(story_id)},
                            {'$set': {'story_img': encoded}}
                        )
    return redirect(url_for('blog', user_id=session['user_id']))

@app.route('/')
def index():
    for user in myusers.find():
        filename = f"{os.path.join(app.config['UPLOAD_FOLDER'], '{}.png'.format(user['user_id']))}"
        if(not os.path.isfile(filename)):
            try:
                img = bin_to_image(user['user_img'])
                img.save(filename)
            except Exception:
                ""
    users = myusers.find()
    return render_template(
        "index.html",
        users=users
    )

@app.route('/login')
def login():
    if 'user_id' in session:
        if session['user_id'] == ADMIN.ID:
            return redirect(url_for('admin'))
        return redirect(url_for('blog', user_id=session['user_id']))
    return render_template('login.html')

@app.route('/login/submit', methods=['POST', 'GET'])
def login_submit():
    if request.method == 'POST':
        user_id = request.form['id']
        user_password = request.form['password']
        if user_id == ADMIN.ID and user_password == ADMIN.PASS:
            session['user_id'] = ADMIN.ID
            return redirect(url_for('admin'))
        user = myusers.find_one({
            'user_id': user_id,
            'password': user_password
        })
        if user is not None:
            session['user_id'] = user['user_id']
            session['user_name'] = user['user_name']
            return redirect(url_for('blog', user_id=user['user_id']))
    return redirect(url_for('login'))

@app.route('/logout')
def logout():
    session.pop('user_id', None)
    session.pop('user_name', None)
    return redirect(url_for('login'))

@app.route('/blog/<user_id>/<story_id>/new/paragraph')
def new_paragraph(user_id, story_id):
    user = myusers.find_one({'user_id': user_id})
    story = mystories.find_one({'_id': ObjectId(story_id)})
    return render_template(
        'new_paragraph.html',
        user=user,
        story=story
    )

@app.route('/blog/<user_id>/<story_id>/new/paragraph/submit', methods=['POST'])
def new_paragraph_submit(user_id, story_id):
    if request.method == 'POST':
        user = myusers.find_one({'user_id': user_id})
        story = mystories.find_one({'_id': ObjectId(story_id)})
        paragraph = {
            'story_id': story_id,
            'paragraph': request.form['paragraph'],
            'timestamp': current_timestamp()
        }
        myparagraphs.insert_one(paragraph)
    return redirect(url_for(
        'view_blog',
        user_id=user['user_id'],
        story_id='{}'.format(story['_id']))
    )


@app.route('/new_user', methods=['POST', 'GET'])
def new_user():
    if request.method == 'POST':
        user_id = request.form['id']
        user_name = request.form['name']
        user = {
            'user_id': user_id,
            'user_name': user_name
        }
        users = myusers.find({'user_id': user_id})
        if len(list(users)) > 0:
            return redirect(url_for('admin'))
        x = myusers.insert_one(user)
        password = '{}'.format(x.inserted_id)
        password = password[-6:]
        myusers.update_one(
            {'user_id': user_id},
            {'$set': {
                'password': password
            }}
        )
    return redirect(url_for('admin'))

@app.route('/blog/new_story')
def new_story():
    if 'user_id' not in session:
        return redirect(url_for('login'))
    return render_template('new_story.html')

@app.route('/blog/new_story/submit', methods=['POST', 'GET'])
def new_story_submit():
    if request.method == 'POST':
        title = request.form['title']
        description = request.form['description']
        story_type = request.form['story_type']
        story = {
            'user_id': session['user_id'],
            'title': title,
            'description': description,
            'stype': story_type
        }
        x = mystories.insert_one(story)
        if 'file' in request.files:
            file = request.files['file']
            if file.filename != '':
                if file and check_if_allowed_file(file.filename):
                    filename = secure_filename(f'{x.inserted_id}.png')
                    filepath = os.path.join(
                        app.config['UPLOAD_FOLDER'],
                        filename
                    )
                    file.save(filepath)
                    with open(filepath, 'rb') as f:
                        encoded = Binary(f.read())
                        mystories.update_one(
                            {'_id': ObjectId(x.inserted_id)},
                            {'$set': {'story_img': encoded}}
                        )
    return redirect(url_for('blog', user_id=session['user_id']))

@app.route('/blog/<user_id>/<story_id>')
def view_blog(user_id, story_id):
    user = myusers.find_one({'user_id': user_id})
    story = mystories.find_one(
        {'_id': ObjectId(story_id)},
        {
            'title': 1,
            '_id': 1,
            'description': 1
        }
    )
    paragraphs = myparagraphs.find({'story_id': f"{story['_id']}"})
    return render_template(
        "view_blog.html",
        user=user,
        story=story,
        paragraphs=paragraphs
    )

def get_file_extension(filename):
    fliename = filename.split('.')
    return fliename[len(fliename)-1].lower()

def check_if_allowed_file(filename):
    return '.' in filename and get_file_extension(filename) in ALLOWED_EXTENSIONS

def bin_to_image(binary):
    return Image.open(BytesIO(binary))

def current_timestamp():
    return datetime.datetime.now()

if __name__ == "__main__":
    app.run('0.0.0.0', debug=True, port=5002)